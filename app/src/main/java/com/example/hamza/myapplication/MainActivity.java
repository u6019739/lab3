package com.example.hamza.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.xmlpull.v1.XmlSerializer;

import java.io.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btn;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btn0;
    Button minus;

    @Override
    public void onClick(View v) {
        Button button=(Button)v;
        String string=button.getText().toString();
        if(string.equals("Save")) {
            String line = txv.getText().toString();
            Storagexml storagexml = new Storagexml();
            storagexml.SaveData(line, this);
        }
        if(string.equals("Load"))
        {
            Storagexml storagexml = new Storagexml();
            String st=storagexml.LoadData(this);
            txv.setText(st);
        }
    }

    Button plus;
    Button mul;
    Button divide;
    Button equal;
    Button save;
    Button load;
    float num1 = (0.0f), num2 = (0.0f), result = (0.0f);
    TextView txv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button) findViewById(R.id.button3);
        btn2 = (Button) findViewById(R.id.button4);
        btn3 = (Button) findViewById(R.id.button5);
        btn4 = (Button) findViewById(R.id.button7);
        btn5 = (Button) findViewById(R.id.button8);
        btn6 = (Button) findViewById(R.id.button9);
        btn7 = (Button) findViewById(R.id.button11);
        btn8 = (Button) findViewById(R.id.button12);
        btn9 = (Button) findViewById(R.id.button13);
        btn0 = (Button) findViewById(R.id.button14);
        minus = (Button) findViewById(R.id.button6);
        plus = (Button) findViewById(R.id.button2);
        mul = (Button) findViewById(R.id.button10);
        divide = (Button) findViewById(R.id.button15);
        equal = (Button) findViewById(R.id.button17);
        txv = (TextView) findViewById(R.id.textView);
        save = (Button) findViewById(R.id.button18);
        load = (Button) findViewById(R.id.button19);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "1";
                txv.setText(line);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            //@Override

            //            StringBuilder stringBuilder = new StringBuilder();
//
//            stringBuilder.append("Some text");
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "2";
                txv.setText(line);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "3";
                txv.setText(line);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "4";
                txv.setText(line);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "5";
                txv.setText(line);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "6";
                txv.setText(line);
            }
        });
        btn7. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "7";
                txv.setText(line);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "8";
                txv.setText(line);
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "9";
                txv.setText(line);
            }
        });
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                line = line + "0";
                txv.setText(line);
            }
        });
        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txv.setText("");
            }
        });
        load.setOnClickListener(this);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String line = txv.getText().toString();
                num2 = Float.parseFloat(line);
                num1 += num2;
                line = "";
                txv.setText(line);
            }
        });
        save.setOnClickListener( this);
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String line = new String(txv.getText().toString());
//                SharedPreferences.Editor editor = getSharedPreferences("any name", MODE_PRIVATE).edit();
//                editor.putString("name", line);
//                editor.commit();
//            }
//        });
//        load.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v) {
//                SharedPreferences pref = getApplicationContext().getSharedPreferences("any name", MODE_PRIVATE);
//                txv.setText(pref.getString("name", null));
//            }
//        });
    }
}



package com.example.hamza.myapplication;




import android.app.Activity;
import android.util.Xml;
import android.view.View;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Administrator on 2017/3/26.
 */

public class Storagexml {

    public void SaveData(String string, Activity activity) {
        writeToXml(WriteToString(string),activity);
    }

    public String LoadData(Activity activity) {
        String string = ReadXmlUser("",activity);
        return string;
    }

    private String WriteToString(String text) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("utf-8", true);
            serializer.startTag("", "content");
            serializer.text(text);
            serializer.endTag("", "content");
            serializer.endDocument();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    private boolean writeToXml(String str, Activity activity) {
        try {
            OutputStream out = activity.openFileOutput("users.xml", MODE_PRIVATE);
            OutputStreamWriter outWriter = new OutputStreamWriter(out);
            try {
                outWriter.write(str);
                outWriter.close();
                out.close();
                return true;
            } catch (IOException e) {

                return false;
            }
        } catch (Exception e) {

            return false;
        }
    }


    private String ReadXmlUser(String tag,Activity activity) {
        String re = "";
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;
        Document document;
        try {
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(activity.openFileInput("users.xml"));
            org.w3c.dom.Element root = document.getDocumentElement();
            Node node = root.getFirstChild();
            re = node.getNodeValue();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            document = null;
            documentBuilder = null;
            documentBuilderFactory = null;
        }
        return re;
    }
}

